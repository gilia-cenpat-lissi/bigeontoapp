<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel='stylesheet' href='css/bootstrap.min.css'>
    <link rel='stylesheet' href='css/micss.css'>
    <title>LinkedFAI</title>
    </head>
    
    <body>
        <div class="wrap">
            <div id="main">
                <div class="row">
                    <div class="column"><img src="src/logofai.png" class="logo"/></div>
                    <div class="column"><h1>Linked FAI</h1></div> 
                    <div class="column"><img src="src/comahue.png" class="logo"/></div>
                </div>
                <hr>
                <div class="container-fluid h-100">
                    <div class="row w-100 align-items-center">
                        <div class="col text-center">
                            <p><button id="d2r" type="button" onclick="abrirLinkedGilia()" class="btn btn-primary align-items-center  btn-lg" style='width:400px; height:200px'>LinkedGILIA</button></p>
                            <p><button type="button" onclick="abrirOpenRefine()" class="btn btn-secondary btn-lg" style='width:400px; height:200px'>FAI Enlazada</button></p>
                        </div>
                    </div>
                </div>    
                <script src='js/jquery-3.3.1.min.js'></script>
                <script src='js/acciones.js'></script>
                <script src='js/popper.min.js'/></script>
                <script src='js/bootstrap.js'/></script>
            </div>
        </div>
         <!--div id="footer">
         <hr>
            <br>
             Facultad de Informática - Universidad Nacional del Comahue<br>
            Buenos Aires 1400, Neuquén, Argentina
    </div-->
    </body>
    
   
</html>