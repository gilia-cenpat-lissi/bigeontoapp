<?php

require_once("../sparqllib.php");

	class DblpController { 
		
        	public $model; 
                public $uri;

        	public function __construct(DblpModel $model, String $dblpuri) { 
                    $this->model = $model;
                    $this->uri = $dblpuri;
                    
        	} 

        	public function click() { 
        		//echo($this->model->getPublicacionesExternas($this->uri));
        		return $this->model->getPublicacionesExternas($this->uri);
        		
        	} 
                
    	}
    