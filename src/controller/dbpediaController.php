<?php

require_once("../sparqllib.php");

	class DbpediaController { 
		
        	public $model; 

        	public function __construct(DbpediaModel $model) { 
                    $this->model = $model; 
        	} 

        	public function events() { 
        		
        		return $this->model->getEventInYear();
        		
        	} 
        	
        	public function tvShows() {
        	
        		return $this->model->getTVShowInYear();
        	
        	}
                
    	}
    