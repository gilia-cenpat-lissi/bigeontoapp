<?php 

require_once("../sparqllib.php");

	class LocalesController { 
		
        	public $model; 
                public $uri;

        	public function __construct(GiliaModel $model, String $uri) { 
            	$this->model = $model; 
                $this->uri = $uri;
        	} 

        	public function click() { 
        		
        		return $this->model->getPublicacionesLocales($this->uri);
        		
        	} 
    	}
    
    
    
