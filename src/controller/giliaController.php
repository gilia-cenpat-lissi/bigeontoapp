<?php 

require_once("../sparqllib.php");

	class GiliaController { 
		
        	public $model; 

        	public function __construct(GiliaModel $model) { 
            	$this->model = $model; 
        	} 

        	public function click() { 
        		
        		return $this->model->getResearchers();
        		
        	} 
    	}
    
    
    
