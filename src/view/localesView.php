<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Linked GILIA</title>
        <link rel="stylesheet" type="text/css" href="../css/gilia.css"/>
        <script type="text/javascript" src="../js/jquery-1.12.1.min.js"></script>

        <script type="text/javascript">
              $(document).on("click", "#researchers", function(event){
                location.reload();
            });
        
        </script> 
        
    </head>
    <body>
        <div id="wrap">
            <div id="main">
                <img class="logo" src="../logofai.png" alt="Facultad de Informática" title="Facultad de Informática"/>
                <!--img class="logo" src="" alt="Grupo GILIA" title="Grupo de Investigación en Lenguajes e Inteligencia Artificial"/--> <h1 class="titulo">Linked GILIA</h1>
                <hr>
                <ul class="nav">
                    <li><a href="../index.php">Start</a>

                    </li>
                    <li><a id="researchers">Researchers</a></li>
                    <li><a href="">About</a>
                        <ul>
                            <li><a href="">Team</a></li>
                            <li><a href="http://faiweb.uncoma.edu.ar/">Facultad de Informática</a></li>
                            <li><a href="">About group</a></li>
                        </ul>
                    </li>
                    <li><a href="">Contact</a></li>
                    <li><a href="dbpediaView.php" id="stuff">dbpedia stuff</a>
                </ul>
                <br>
                <br>
                <br>
                <h2>Publicaciones Locales</h2>
                <?php 
                        ini_set('display_errors', 'on');
                        require_once '../sparqllib.php';
                        require_once '../model/giliaModel.php';
                        require_once '../controller/localesController.php';
                        


                        $model = new GiliaModel();

                        	//var_dump($model);

                        $controller = new LocalesController($model, $_GET['uri']);
                        

                            $view = new View($controller, $model);

                            $controller->click();
                            $view->display();


                            //	if (isset($_GET['accion'])){
                            //		$controller->{$_GET['accion']}(); 
                            //		$view->display();
                            //	};

                    class View { 
                            private $model; 
                            private $controller; 

                            public function __construct(LocalesController $controller, GiliaModel $model) { 
                                $this->controller = $controller; 
                                $this->model = $model; 
                            } 

                            public function display() { 

                            //	return '<a href="index2.php?accion=click">' . $this->model->result . '</a>';
                                    print "<p>AUTOR: ".$_GET['nombre']."</p>";
                                    //var_dump( $this->model->result );
                                    if(sparql_num_rows( $this->model->result )>0){
                                        $fields = sparql_field_array( $this->model->result );
                                        print "<p>Cantidad de resultados: ".sparql_num_rows( $this->model->result )." .</p>";
                                        print "<table border='1'>";
                                        print "<tr>";
                                        foreach( $fields as $field )
                                        {
                                                print "<th>$field</th>";
                                        }
                                        print "</tr>";
                                        while( $row = sparql_fetch_array( $this->model->result ) )
                                        {
                                                print "<tr>";
                                                foreach( $fields as $field )
                                                {
                                                    if($field=='uri'){
                                                        print "<td><a href='$row[$field]'>$row[$field]</a></td>";
                                                    }
                                                    else{
                                                        if($field=='linkpdf'){                                                           
                                                            print "<td><a href='http://localhost/faienlazada/$row[$field]'>$row[$field]</a></td>";                                                           
                                                        }
                                                        else{
                                                                print "<td>$row[$field]</td>";
                                                            }

                                                        }
                                                    }

                                                }
                                                print "</tr>";
                                        
                                        print "</table>";
                                        }
                                        else{
                                            print "<h2>Este autor no tiene publicaciones locales</h2>";
                                    }
                                   

                            }            
                        }
                    ?>          
            </div>
        </div>
<!--        <div id="main">
            <br>
            <br>
            <br>
            <h2>GILIA researchers</h2>
        </div>-->


        <div id="footer">
           <hr>
            <br>
            Proyecto Web Semántica del grupo GILIA Facultad de Informática - Universidad Nacional del Comahue<br>
            Buenos Aires 1400, Neuquén, Argentina
            
        </div>
    </body>
</html>
