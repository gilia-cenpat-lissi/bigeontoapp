<!DOCTYPE html>
<html>
    <head>
        <!--meta charset="UTF-8"-->
        <title>Linked GILIA</title>
        <link rel="stylesheet" type="text/css" href="../css/gilia.css"/>
        <script type="text/javascript" src="../js/jquery-1.12.1.min.js"></script>

        <script type="text/javascript">
              $(document).on("click", "#researchers", function(event){
                location.reload();
            });
        
        </script> 
        
    </head>
    <body>
        <div id="wrap">
            <div id="main">
                <img class="logo" src="../logofai.png" alt="Facultad de Informática" title="Facultad de Informática"/>
                <!--img class="logo" src="" alt="Grupo GILIA" title="Grupo de Investigación en Lenguajes e Inteligencia Artificial"/--> <h1 class="titulo">Linked GILIA</h1>
                <hr>
                <ul class="nav">
                    <li><a href="../index.php">Start</a>

                    </li>
                    <li><a id="researchers">Researchers</a></li>
                    <li><a href="">About</a>
                        <ul>
                            <li><a href="">Team</a></li>
                            <li><a href="http://faiweb.uncoma.edu.ar/">Facultad de Informática</a></li>
                            <li><a href="">About group</a></li>
                        </ul>
                    </li>
                    <li><a href="">Contact</a></li>
                    <li><a href="dbpediaView.php" id="stuff">dbpedia stuff</a>
                </ul>
                <br>
                <br>
                <br>
                <h2>GILIA researchers</h2>
                <?php 
                        ini_set('display_errors', 'on');
                        require_once '../sparqllib.php';
                        require_once '../model/giliaModel.php';
                        require_once '../controller/giliaController.php';
                        


                        $model = new GiliaModel();

                        	//var_dump($model);

                        $controller = new GiliaController($model);
                        //var_dump($controller);

                            $view = new View($controller, $model);

                            $controller->click();
                            $view->display();

                    class View { 
                            private $model; 
                            private $controller; 

                            public function __construct(GiliaController $controller, GiliaModel $model) { 
                                $this->controller = $controller; 
                                $this->model = $model; 
                            } 

                            public function display() { 

                                    $fields = sparql_field_array( $this->model->result );
                      
                                    print "<p>Number of results: ".sparql_num_rows( $this->model->result )." .</p>";
                                    print "<table border='1'>";
                                    print "<tr>";
                                    foreach( $fields as $field )
                                    {
                                            print "<th>$field</th>";
                                    }
                                    print "<th>Publicaciones Locales</th>";
                                    print "</tr>";
                                    while( $row = sparql_fetch_array( $this->model->result ) )
                                    {
                                            $nombre="";
                                            print "<tr>";
                                            foreach( $fields as $field )
                                            {
                                                if($field=='investigadores'){
                                                    print "<td><a href='$row[$field]'>$row[$field]</a></td>";
                                                    $investigador=$row[$field];
                                                }
                                                else{
                                                    if($field=='publicacionesdblp'){
                                                        if(!empty($row[$field])){
                                                            //$enlace=utf8_encode($row[$field]);
                                                            //echo($enlace);
                                                            print "<td><a href='dblpView.php?link=$row[$field]&name=$nombre'>DBLP</a></td>";
                                                        }
                                                        else{
                                                            print "<td>NO POSEE</td>";
                                                        }
                                                    }
                                                    else{
                                                        if($field=='conicet'){
                                                            if($row[$field]==0){
                                                                print "<td>no</td>";
                                                            }
                                                            else{
                                                                print "<td>si</td>";
                                                            }
                                                        }
                                                        else{
                                                        
                                                            if($field=='nombre'){
                                                                $nombre.= $row[$field]." ";
                                                            }
                                                            if($field=='apellido'){
                                                                $nombre.=$row[$field];
                                                            }
                                                            print "<td>$row[$field]</td>";
                                                        }

                                                    }
                                                }

                                            }
                                         
                                            print "<td><a href='localesView.php?uri=$investigador&nombre=$nombre'=>PDF Locales</a></td>";
                                            
                                            print "</tr>";
                                    }
                                    print "</table>";

                            }            
                        }
                    ?>          
            </div>
        </div>
        <div id="footer">
           <hr>
            <br>
            Proyecto Web Semántica del grupo GILIA Facultad de Informática - Universidad Nacional del Comahue<br>
            Buenos Aires 1400, Neuquén, Argentina
            
        </div>
    </body>
</html>
