<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Linked GILIA</title>
        <link rel="stylesheet" type="text/css" href="../css/gilia.css"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel='stylesheet' href='../css/bootstrap.min.css'>
        <link rel='stylesheet' href='../css/micss.css'>
        <script type="text/javascript" src="../js/jquery-1.12.1.min.js"></script>
    </head>
    <body>
        <div id="wrap">
            <div id="main">
                <img class="logo" src="../logofai.png" alt="Facultad de Informática" title="Facultad de Informática"/>
                <!--img class="logo" src="" alt="Grupo GILIA" title="Grupo de Investigación en Lenguajes e Inteligencia Artificial"/--> <h1 class="titulo">Linked GILIA</h1>
                <hr>
                <ul class="nav">
                    <li><a href="../index.php">Start</a>
                    </li>
                    <li><a href='giliaView.php' id="researchers">Researchers</a></li>
                    <li><a href="">About</a>
                        <ul>
                            <li><a href="">Team</a></li>
                            <li><a href="http://faiweb.uncoma.edu.ar/">Facultad de Informática</a></li>
                            <li><a href="">About group</a></li>
                        </ul>
                    </li>
                    <li><a href="">Contact</a></li>
                    <!--li><a href="dbpediaView.php" id="stuff">dbpedia stuff</a-->
                </ul>
                <br>
                <br>
                <br>
                <h2>Publicaciones en DBLP</h2>
                <?php 
                        require_once '../sparqllib.php';
                        require_once '../model/dblpModel.php';
                        require_once '../controller/dblpController.php';


                        $model = new DblpModel();

                            //	var_dump($model);
                            //echo($_GET['link']);
                            $controller = new DblpController($model, $_GET['link']);
                            

                            //   var_dump($controller);

                            $view = new DblpView($controller, $model);

                            $controller->click();
                            $view->display();


                    class DblpView { 
                            private $model; 
                            private $controller; 

                            public function __construct(DblpController $controller, DblpModel $model) { 
                                $this->controller = $controller; 
                                $this->model = $model; 
                            } 

                            public function display() { 


                                    $fields = sparql_field_array( $this->model->result );
                                    print "<h2>Autor: ".$_GET['name']."</h2>";
                                    print "<p>Cantidad de resultados: ".sparql_num_rows( $this->model->result ).".</p>";
                                    print "<table border='1'>";
                                    print "<tr>";
                                    foreach( $fields as $field )
                                    {                                       
                                        print "<th>$field</th>";
                                    }
                                    print "</tr>";
                                    while( $row = sparql_fetch_array( $this->model->result ) )
                                    {
                                            print "<tr>";
                                            foreach( $fields as $field )
                                            {
                                                if($field=='linkpdf'){
                                                    print "<td><a href='$row[$field]'>Link to paper</a></td>";
                                                }
                                                else{
                                                    if($field=='paper'){
                                                        print "<td><a href='$row[$field]'>$row[$field]</a></td>";
                                                    }
                                                    else{
                                                        print "<td>$row[$field]</td>";
                                                    }

                                                    }                                         
                                            }
                                            print "</tr>";
                                    }
                                    print "</table>";

                            }            
                        }
                    ?>
            </div>
        </div>
         <div id="footer">
           <hr>
            <br>
            Proyecto Web Semántica del grupo GILIA Facultad de Informática - Universidad Nacional del Comahue<br>
            Buenos Aires 1400, Neuquén, Argentina
            
        </div>
    </body>
</html>