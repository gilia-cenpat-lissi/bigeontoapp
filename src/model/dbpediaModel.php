<?php
require_once("../sparqllib.php");
 
 class DbpediaModel { 
 	 
        public $db_connection;
        public $result;

        
        public function __construct() { 
        	
        	$ini_array = parse_ini_file("../linkeddata_config.ini", true);
                $db = sparql_connect($ini_array["SPARQL_endpoints"]["url"]["dbpedia"]);
	    	if( !$db ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		sparql_ns( "foaf","http://xmlns.com/foaf/0.1/" );
        		sparql_ns( "dc","http://purl.org/dc/elements/1.1/" );
        		sparql_ns("vocab", "localhost:2020#");
        		sparql_ns("dbo", "http://dbpedia.org/ontology/");
//                        sparql_ns("dcterms","http://purl.org/dc/terms/");
//                        sparql_ns("xsd","http://www.w3.org/2001/XMLSchema#");
//                        sparql_ns("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
                sparql_ns("rdf","http://www.w3.org/1999/02/22-rdf-syntax-ns#");
//                        sparql_ns("owl","http://www.w3.org/2002/07/owl#");
        	
        		$this->db_connection = $db;
                        //var_dump($db);
        	};
        }
        
        
        public function getEventInYear() {
        	 
        	$sparql = "SELECT ?event ?date
						WHERE {
        					?event rdf:type dbo:Event .
   							?event dbo:startDate ?date .
   							FILTER (str(?date) >= '2015-01-01')
						} LIMIT 10";
        	 
        	$result = sparql_query($sparql);
        
        	if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        		 
        	}
        	 
        }
        
        public function getTVShowInYear() {
        
        	$sparql = "SELECT distinct ?show ?date 
						WHERE {
        					?show rdf:type dbo:TelevisionShow .
   							?show dbo:releaseDate ?date .
   							FILTER (str(?date) >= '2015-01-01')
						} LIMIT 10";
        
        	$result = sparql_query($sparql);
        
        	if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        		 
        	}
        
        }
        

        
        
} 