 <?php
 

 require_once("../sparqllib.php");
 
 class GiliaModel { 
 	 
        public $db_connection;
        public $result;

        
        public function __construct() { 
        	
            //$ini_array = parse_ini_file("../linkeddata_config.ini", true);
            //$db = sparql_connect($ini_array["SPARQL_endpoints"]["url"]["local"]);
              $db = sparql_connect("http://localhost:2020/sparql");
	    if( !$db ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
            else {
        	sparql_ns("foaf","http://xmlns.com/foaf/0.1/" );
        	sparql_ns("dc","http://purl.org/dc/elements/1.1/" );
        	sparql_ns("dblp", "https://dblp.org/rdf/schema-2017-04-18#");
                sparql_ns("vocab","http://localhost:2020#");
        	
        		$this->db_connection = $db;
                        //var_dump($db);
        	};
        	
        }
        
        
        public function getResearcherName() {
        	
        	$sparql = "SELECT ?firstname ?surname
  						WHERE {
  							<http://localhost:2020/resource/researcher/1> foaf:firstname  ?firstname .
							<http://localhost:2020/resource/researcher/1> foaf:surname ?surname 
        				}";
        	
        	$result = sparql_query($sparql);
        	 
        	if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        	
        	}
        	
        }
        
        
        public function getResearcherCat() {
        	 
        	$sparql = "SELECT ?surname ?category 
  						WHERE {
  							<http://localhost:2020/resource/researcher/1> vocab:category ?category .
							<http://localhost:2020/resource/researcher/1> foaf:surname ?surname
        				}";
        	 
        	$result = sparql_query($sparql);
        
        	if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        		 
        	}
        	 
        }
        
        
        public function getResearcherConicet() {
        
        	$sparql = "SELECT ?surname ?conicet
  						WHERE {
  							<http://localhost:2020/resource/researcher/1> vocab:conicet  ?conicet .
							<http://localhost:2020/resource/researcher/1> foaf:surname ?surname
        				}";
        
        	$result = sparql_query($sparql);
        
        	if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        		 
        	}
        
        }
        
        
        public function getResearcherTitle() {
        
        	$sparql = "SELECT ?surname ?title
  						WHERE {
  							<http://localhost:2020/resource/researcher/1> foaf:title  ?title .
							<http://localhost:2020/resource/researcher/1> foaf:surname ?surname
        				}";
        
        	$result = sparql_query($sparql);
        
        	if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        		 
        	}
        
        }
        
        public function getResearchers(){
            $sparql= "SELECT ?URI ?Name ?Surname ?Title ?CONICET ?Category ?dblp
                                WHERE {
                                   ?URI foaf:firstname ?Name .
                                  ?URI foaf:surname ?Surname .
                                  ?URI foaf:title ?Title .
                                  ?URI vocab:category ?Category .
                                  ?URI vocab:conicet ?CONICET .
                                OPTIONAL
                                {
                                  ?PUB vocab:filenum ?URI .
                                  ?PUB vocab:uri ?dblp
                                }
                                }";
            
            $result= sparql_query($sparql);
          //  var_dump($result);
            
            if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        		
        	}
            
            
        }
        
        
        public function getDBLPuri () {
    		
        	$sparql = "SELECT ?s ?dblp 
        			   	WHERE {
  								?s vocab:filenum <http://localhost:2020/resource/researcher/1> .
  								?s vocab:uri ?dblp
					    }";
        	
        	$result = sparql_query($sparql);
        	//var_dump($result);
        	
        	if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        		
        	}
        	
        }
        
         
} 
