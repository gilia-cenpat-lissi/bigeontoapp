<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
require_once("../sparqllib.php");
 
 class DblpModel { 
 	 
        public $db_connection;
        public $result;

        
        public function __construct() { 
        	
        	$ini_array = parse_ini_file("../linkeddata_config.ini", true);
                $db = sparql_connect($ini_array["SPARQL_endpoints"]["url"]["l3s"]);
	    	if( !$db ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		sparql_ns( "foaf","http://xmlns.com/foaf/0.1/" );
        		sparql_ns( "dc","http://purl.org/dc/elements/1.1/" );
        		sparql_ns("vocab", "localhost:2020#");
//                        sparql_ns("dcterms","http://purl.org/dc/terms/");
//                        sparql_ns("xsd","http://www.w3.org/2001/XMLSchema#");
//                        sparql_ns("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
//                        sparql_ns("rdf","http://www.w3.org/1999/02/22-rdf-syntax-ns#");
//                        sparql_ns("owl","http://www.w3.org/2002/07/owl#");
        	
        		$this->db_connection = $db;
                        //var_dump($db);
        	};
        }
        
        
        public function getResearcherName($uri) {//hay que concatenar el id o nombre del autor
        	
        	$sparql = "SELECT ?firstname ?surname
  						WHERE {
  							<http://dblp.l3s.de/d2r/page/authors/> foaf:firstname  ?firstname .
							<http://localhost:2020/resource/researcher/1> foaf:surname ?surname 
        				}";
        	
        	$result = sparql_query($sparql);
        	 
        	if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        	
        	}
        	
        }
        
        
        public function getResearcherCat($uri) {
        	 
        	$sparql = "SELECT ?surname ?category 
  						WHERE {
  							<http://localhost:2020/resource/researcher/1> vocab:category ?category .
							<http://localhost:2020/resource/researcher/1> foaf:surname ?surname
        				}";
        	 
        	$result = sparql_query($sparql);
        
        	if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        		 
        	}
        	 
        }
        
        
        public function getResearcherConicet($uri) {
        
        	$sparql = "SELECT ?surname ?conicet
  						WHERE {
  							<http://localhost:2020/resource/researcher/1> vocab:conicet  ?conicet .
							<http://localhost:2020/resource/researcher/1> foaf:surname ?surname
        				}";
        
        	$result = sparql_query($sparql);
        
        	if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        		 
        	}
        
        }
        
        
        public function getResearcherTitle($uri) {
        
        	$sparql = "SELECT ?surname ?title
  						WHERE {
  							<http://localhost:2020/resource/researcher/1> foaf:title  ?title .
							<http://localhost:2020/resource/researcher/1> foaf:surname ?surname
        				}";
        
        	$result = sparql_query($sparql);
        
        	if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        		 
        	}
        
        }
        
        public function getResearchers(){
            $sparql= "SELECT DISTINCT ?URI ?Name ?Surname ?Title ?CONICET ?Category ?dblp WHERE {
                                        ?URI foaf:firstname ?Name .
                                        ?URI foaf:surname ?Surname .
                                        ?URI foaf:title ?Title .
                                        ?URI vocab:category ?Category .
                                        ?URI vocab:conicet ?CONICET .
                                        ?PUB vocab:filenum ?URI .
                                        ?PUB vocab:uri ?dblp
                                      }";
            $result= sparql_query($sparql);
            
            //var_dump($result);
            
            if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        		
        	}
            
            
        }
        
        
        public function getDBLPuri ($filenum) {
    		
        	$sparql = "SELECT ?s ?dblp 
        			   	WHERE {
  								?s vocab:filenum <http://localhost:2020/resource/researcher/1> .
  								?s vocab:uri ?dblp
					    }";
        	
        	$result = sparql_query($sparql);
        	//var_dump($result);
        	
        	if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        		
        	}
        	
        }
        
        public function getArt($uri){
  
        $sparql=   "SELECT ?paper ?title ?name ?pdf
                     WHERE {"." <".$uri.">"." foaf:name ?name .
                               ?paper foaf:maker "."<".$uri.">"." .
                               ?paper dc:title ?title .
                               ?paper foaf:homepage ?pdf
                                  }";
        $result = sparql_query($sparql);
     
        if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        else{
                return $this->result = $result;
            }
        
        }
        
         public function getPublicacionesExternas($dblpuri){
            $sparql="SELECT ?paper ?titulo ?linkpdf WHERE{"
                    . "                                    ?paper dc:creator <$dblpuri> ."
                    . "                                    ?paper dc:title ?titulo ."
                    . "                                    ?paper foaf:homepage ?linkpdf }";
            $dblp=sparql_connect("https://dblp.l3s.de/d2r/sparql");
	    if( !$dblp ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
            else {
        	sparql_ns("foaf","http://xmlns.com/foaf/0.1/" );
        	sparql_ns("dc","http://purl.org/dc/elements/1.1/" );
                sparql_ns("vocab","localhost:2020/");
                sparql_ns("owl","http://www.w3.org/2002/07/owl#");
                $result=sparql_query($sparql);
                if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
                        return $this->result = $result;
        		
                      }
                }
        }
        
        
        
} 