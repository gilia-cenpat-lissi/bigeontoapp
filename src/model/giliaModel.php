 <?php
 
error_reporting(E_ALL);
 ini_set('display_errors', '1');
 require_once("../sparqllib.php");
 
 class GiliaModel { 
 	 
        public $db_connection;
        public $result;

        
        public function __construct() { 
        	
            //$ini_array = parse_ini_file("../linkeddata_config.ini", true);
            //$db = sparql_connect($ini_array["SPARQL_endpoints"]["url"]["local"]);
            $db = sparql_connect("http://localhost:2020/sparql");
	    if( !$db ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
            else {
        	sparql_ns("foaf","http://xmlns.com/foaf/0.1/" );
        	sparql_ns("dc","http://purl.org/dc/elements/1.1/" );
                sparql_ns("vocab","localhost:2020/");
                sparql_ns("owl","http://www.w3.org/2002/07/owl#");
                
        	
        		$this->db_connection = $db;
                        //var_dump($db);
        	};
        	
        }
        
        
        public function getResearcherName() {
        	
        	$sparql = "SELECT ?firstname ?surname
  						WHERE {
  							<http://localhost:2020/resource/researcher/1> foaf:firstname  ?firstname .
							<http://localhost:2020/resource/researcher/1> foaf:surname ?surname 
        				}";
        	
        	$result = sparql_query($sparql);
        	 
        	if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        	
        	}
        	
        }
        
        
        public function getResearcherCat() {
        	 
        	$sparql = "SELECT ?surname ?category 
  						WHERE {
  							<http://localhost:2020/resource/researcher/1> vocab:category ?category .
							<http://localhost:2020/resource/researcher/1> foaf:surname ?surname
        				}";
        	 
        	$result = sparql_query($sparql);
        
        	if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        		 
        	}
        	 
        }
        
        
        public function getResearcherConicet() {
        
        	$sparql = "SELECT ?surname ?conicet
  						WHERE {
  							<http://localhost:2020/resource/researcher/1> vocab:conicet  ?conicet .
							<http://localhost:2020/resource/researcher/1> foaf:surname ?surname
        				}";
        
        	$result = sparql_query($sparql);
        
        	if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        		 
        	}
        
        }
        
        
        public function getResearcherTitle() {
        
        	$sparql = "SELECT ?surname ?title
  						WHERE {
  							<http://localhost:2020/resource/researcher/1> foaf:title  ?title .
							<http://localhost:2020/resource/researcher/1> foaf:surname ?surname
        				}";
        
        	$result = sparql_query($sparql);
        
        	if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        		 
        	}
        
        }
        
        public function getResearchers(){
            /*$sparql= " SELECT ?URI ?Name ?Surname ?Title ?CONICET ?Category ?dblp
                                WHERE {
                                  ?URI foaf:firstname ?Name .
                                  ?URI foaf:surname ?Surname .
                                  ?URI foaf:title ?Title .
                                  ?URI vocab:category ?Category .
                                  ?URI vocab:conicet ?CONICET .
                                OPTIONAL
                                {
                                  ?PUB vocab:filenum ?URI .
                                  ?PUB vocab:uri ?dblp
                                }
                                }";*/
            /*$sparql="SELECT ?nombre ?apellido ?conicet ?cat ?investigador ?publicacionesdblp WHERE {
                                ?uris owl:sameAs ?investigador .
                                ?investigador foaf:firstname ?nombre .
                                ?investigador foaf:surname ?apellido .
                                ?investigador vocab:conicet ?conicet .
                                ?investigador vocab:categoria ?cat .
                                ?uris vocab:uridblp ?publicacionesdblp
                                }

                                ";*/
            $sparql= "SELECT  ?nombre ?apellido ?titulo ?conicet ?cat ?investigadores ?publicacionesdblp
                                WHERE {
                                  ?investigadores foaf:firstname ?nombre .
                                  ?investigadores foaf:surname ?apellido .
                                  ?investigadores foaf:title ?titulo .
                                  ?investigadores vocab:categoria ?cat .
                                  ?investigadores vocab:conicet ?conicet .
                                OPTIONAL
                                {
                                  ?PUB owl:sameAs ?investigadores .
                                  ?PUB vocab:uridblp ?publicacionesdblp
                                }}";
            
  
            $result= sparql_query($sparql);
            
            if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        		
        	}
            
            
        }
        
        //$uri debe ser la uri local de los investigadores
        public function getPublicacionesLocales($uri){
            $sparql="SELECT ?uri ?titulo ?linkpdf WHERE {?uri dc:creator <$uri> ."
                    . "                                  ?uri dc:title ?titulo ."
                    . "                                  ?uri foaf:homepage ?linkpdf }";
            $result=sparql_query($sparql);
            if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        		
        	}
        }
        
        
        //obtiene remotamente el nombre y apellido de los investigadores del link consultado
        public function getNombreRemoto($dblpuri){
            $sparql="SELECT ?nombre ?apellido WHERE{"
                    . "                                   <$dblpuri> foaf:firstname ?nombre ."
                    . "                                   <$dblpuri> foaf:surname ?apellido ";
            $dblp=sparql_connect("https://dblp.l3s.de/d2r/sparql");
	    if( !$dblp ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
            else {
        	sparql_ns("foaf","http://xmlns.com/foaf/0.1/" );
                $result=$dblp->sparql_query($sparql);
                if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
                        return $this->result = $result;
        		
                      }
                }
        }
        
        //$dblpuri debe ser la uri de dblp de cada investigador.
        //obtiene las publicaciones de dblp de los investigadores consultados
        public function getPublicacionesExternas($dblpuri){
            $sparql="SELECT ?paper ?titulo ?linkpdf WHERE{"
                    . "                                    ?paper dc:creator <$dblpuri>"
                    . "                                    ?paper dc:title ?titulo ."
                    . "                                    ?paper foaf:homepage ?linkpdf ";
            $dblp=sparql_connect("https://dblp.l3s.de/d2r/sparql");
	    if( !$dblp ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
            else {
        	sparql_ns("foaf","http://xmlns.com/foaf/0.1/" );
        	sparql_ns("dc","http://purl.org/dc/elements/1.1/" );
                sparql_ns("vocab","localhost:2020/");
                sparql_ns("owl","http://www.w3.org/2002/07/owl#");
                $result=$dblp->sparql_query($sparql);
                if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
                        return $this->result = $result;
        		
                      }
                }
        }
        
        
        public function getDBLPuri () {
    		
        	$sparql = "SELECT ?s ?dblp 
        			   	WHERE {
  						?s vocab:filenum <http://localhost:2020/resource/researcher/1> .
                                                ?s vocab:uri ?dblp
					    }";
        	var_dump($sparql);
        	$result = sparql_query($sparql);
        	var_dump($result);
        	
        	if( !$result ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
        	else {
        		return $this->result = $result;
        		
        	}
        	
        }
        
         
} 
