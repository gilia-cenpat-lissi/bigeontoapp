<!DOCTYPE html>
<html>
    <header>
        <title>Open Refine</title>
    </header>
    <body>
        <?php
            //Activar el detector de errores
            error_reporting(E_ALL);
            ini_set('display_errors', '1');
             
            //Linkeamos el Composer para que cargue las librerías de ARC2 y de BorderCloud
            use BorderCloud\SPARQL\SparqlClient; //esto es necesario para que nuestro php Utilice el objeto SparqlClient()
            require __DIR__ . '/vendor/autoload.php';
            
            //incluimos sparqllib para utilizar alguna funcionalidad de esta libreria
            include('sparqllib.php');
            
            
            /*ARC2 simula una base de datos RDF utilizando una base de datos relacional para ello primero debemos 
             * crear una base de datos en nuestro gestor de base de datos relacional local. El gestor debe ser MySQL o
             * MARIADB, no admite MySQL por encima de la version 8.0.
             * 1- Cargaremos en un arreglo en la variable $config, toda la información referida a nuestra base de datos local en este caso llamada opendb.
             */
            $config = array(
            'db_host'=>'127.0.0.1',                  //localhost
            'db_name'=>'opendb',                     //nombre de la base de datos
            'db_user'=>'root',                       //usuario de la base de datos
            'db_pwd'=>'aterrador00',                 //contraseña de la base de datos
            'store_name'=>'arc_tests',               //nombre de almacenamiento de triplas
            'bnode_prefix'=>'bn',                    //prefijo del nodo raíz o base
            'sem_html_formats'=>'rdfa microformats', //formato de los elementos a almacenar
                );
    
            /*2- Referencio la base de datos a un objeto Store en la variable $store y la enciendo si aún no lo está. */
            $store = ARC2::getStore($config);
            if(!$store->isSetUp())
                {
                    $store->setUp();
                }
                
            /*3- Este método se ejecuta una sola vez para cargar la información contenida en el archivo creado en OpenRefine, pruebaturtle.ttl, en
             *la base de datos local. La función corresponde a la función SPARQL LOAD que carga un archivo a la base de datos RDF. ARC2 la simula muy 
             *bien*/
            $q= 'LOAD <http://localhost/faienlazada/investigadores_publicaciones.ttl>';
            $store->query($q);
            $q= 'LOAD <http://localhost/faienlazada/publicacionesturtle.ttl>';
            $store->query($q);
                
            /*4- Realizo consultas sobre la base de datos cargada en ARC2. 
             *Recordar siempre de agregar los prefijos de nuestro vocabulario a las consultas.
             *El DISTINCT, al igual que en SQL, nos ayuda a eliminar las triplas repetidas.*/
                
            $q='PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>
            PREFIX owl: <http://www.w3.org/2002/07/owl#>
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX foaf: <http://xmlns.com/foaf/0.1/>
            PREFIX dc: <http://purl.org/dc/elements/1.1/>
            PREFIX dblp: <https://dblp.org/rdf/schema-2017-04-18#> SELECT DISTINCT ?n WHERE {?o foaf:name ?n}';
            /*Si no hay errores en la consulta se muestra el resultado como un listado*/
            $r='';
            if ($rows = $store->query($q, 'rows'))
            {
                foreach ($rows as $row) 
                    {
                        $r .= '<li>' . $row['n'] . '</li>';
                    } 
            }
            echo $r;
            
            /*Para realizar consultas a terminales SPARQL remotos, podemos utilizar las librerias sparqllib.php o BorderCloud. 
            *Como utilizamos sparqllib.php en la etapa del servidor D2R, ahora decimos usar BorderCloud*/
            /*A continuación vemos un ejemplo de consulta en un sparql endpoint remoto correspondiente a dblp que devuelve
             *todas las publicaciones realizadas por Laura Cecchi que están almacenadas en el servidor DBLP de l3s*/
            
            $terminal = "https://dblp.l3s.de/d2r/sparql";
            $fuente = new SparqlClient();
            $fuente->setEndpointRead($terminal);
            $consulta= "PREFIX dc: <http://purl.org/dc/elements/1.1/> SELECT *  WHERE {?s dc:creator <https://dblp.l3s.de/d2r/resource/authors/Laura_Andrea_Cecchi> } LIMIT 5";
            $filas=$fuente->query($consulta,'result');
            $error= $fuente->getErrors();
            if($error){
                        print_r($error);
                        throw new Exception(print_r($error, true));
                       }
            else{
                    var_dump($filas);//modificar la forma de ver las filas.
                }
                
                
            /*Ejemplo de la consulta anterior realizada utilizando sparqllib.php*/
                
            $db = sparql_connect("https://dblp.l3s.de/d2r/sparql");
	    if( !$db ) { print sparql_errno() . ": " . sparql_error(). "\n"; exit; }
            else 
                {
                    sparql_ns("foaf","http://xmlns.com/foaf/0.1/" );
                    sparql_ns("dc","http://purl.org/dc/elements/1.1/" );
                    sparql_ns("dblp","https://dblp.org/rdf/schema-2017-04-18#");
                    //var_dump($db);
        	};
            $sparql = "PREFIX dc: <http://purl.org/dc/elements/1.1/> SELECT *  WHERE {?s dc:creator <https://dblp.l3s.de/d2r/resource/authors/Laura_Andrea_Cecchi> } LIMIT 5";
            $result = sparql_query($sparql);
            if( !$result ) 
                { 
                    print sparql_errno() . ": " . sparql_error(). "\n"; exit; 
                }
        	else 
                    {
                        var_dump($result);
                    }       
            ?>
    </body>
</html>



