<?php

/*Esta función crea un string con etiquetas html par cargar una selectora en html con los profesores investigadores de la 
 * base de datos local */
    //error_reporting(E_ALL);
    //ini_set('display_errors', '1');
             
    //Linkeamos el Composer para que cargue las librerías de ARC2 y de BorderCloud
    use BorderCloud\SPARQL\SparqlClient; //esto es necesario para que nuestro php Utilice el objeto SparqlClient()
    require __DIR__ . '/vendor/autoload.php';

function getSelectoraAutores(){
    $selectora= '<select name="select">'.PHP_EOL;
    $consulta='PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>
               PREFIX owl: <http://www.w3.org/2002/07/owl#>
               PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
               PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
               PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
               PREFIX foaf: <http://xmlns.com/foaf/0.1/>
               PREFIX dc: <http://purl.org/dc/elements/1.1/>
               PREFIX dblp: <https://dblp.org/rdf/schema-2017-04-18#> SELECT DISTINCT ?o ?n WHERE {?o foaf:name ?n} '; //en ?o va la URI local y en ?n el nombre del investigador
    
    $resultado = getLocal($consulta);//d[evuelve el arreglo con los resultados de la consulta en donde [n]=> accede al nombre del investigador y [o]=> a la URI local
    
    foreach ($resultado as $row){
        //print_r($row['o']);
        //$o=$row['o'];
        //$n=$row['n'];
        //echo $o.'  '.$n;
        
        $selectora.='<option value="'.$row['o'].'_'.$row['n'].'">'.$row['n'].'</option>'.PHP_EOL;
        
    }
    $selectora.='</select>';
    //var_dump($selectora);
    return $selectora;
    
}


/*Realiza una consulta en la base de datos local. Recibe como parámetro el string de la consulta*/
function getLocal($consulta){
    $respuesta= '<p>No existen publicaciones locales</p>';
    /*Me conecto con la base de datos RDF local donde están almacenados los datos de los investigadores de la FAI*/
    $config = array(
            'db_host'=>'127.0.0.1',                  //localhost
            'db_name'=>'opendb',                     //nombre de la base de datos
            'db_user'=>'root',                       //usuario de la base de datos
            'db_pwd'=>'aterrador00',                 //contraseña de la base de datos
            'store_name'=>'arc_tests',               //nombre de almacenamiento de triplas
            'bnode_prefix'=>'bn',                    //prefijo del nodo raíz o base
            'sem_html_formats'=>'rdfa microformats', //formato de los elementos a almacenar
                );
    
            /* Referencio la base de datos a un objeto Store en la variable $store y la enciendo si aún no lo está. */
            $store = ARC2::getStore($config);
            if(!$store->isSetUp())
                {
                    $store->setUp();
                }
    
            if ($rows = $store->query($consulta, 'rows'))//si funciona el método
            {
                //var_dump($rows);
                $respuesta= $rows;
            }    
                
    return $respuesta;
                
    
}


function getPublicaciones($uri){
    //query Local
    $tablaLocal= '<h2>Publicaciones locales</h2>'.PHP_EOL;
    //query remota
    $tablaRemota='<h2>Publicaciones Remotas</h2>'.PHP_EOL;
    $dblpUri='';//variable que almacena el link de dblp para poder realizar consultas externas
    $qdblp="PREFIX owl: <http://www.w3.org/2002/07/owl#> SELECT DISTINCT ?dblpUri WHERE { <$uri> owl:sameAs ?dblpUri }";//query para obtener el link dblp
    $rdblp= getLocal($qdblp);//solo obtengo la dblp uri
    //var_dump($rdblp);
    if(strcmp (strval($rdblp),'<p>No existen publicaciones locales</p>')!=0){
        foreach($rdblp as $rows){
        $dblpUri=$rows['dblpUri'];
        }
    }
    //consulta que buscara las publicaciones lolcales de acuerdo a la uri local $uri de cada investigador
    $ql ="PREFIX dc:<http://purl.org/dc/elements/1.1/> PREFIX foaf:<http://xmlns.com/foaf/0.1/> SELECT DISTINCT ?plocales ?titulo ?link"
            . " WHERE { <$uri> dc:creator ?plocales . "
                    . " ?plocales dc:title ?titulo ."
                    . " ?plocales foaf:homepage ?link}";
    $rl= getLocal($ql);
    //var_dump($rl);
    //if(strcmp ( getType($rl),'<p>No existen publicaciones locales</p>')!=0){
    if(strcmp ( getType($rl),'string')!=0){
    //if($rl!==null){
        //$tablaLocal.="<table border=1><tr><th>Título</th><th>URI</th><th>Link publicación</th></tr>".PHP_EOL; usar este para mostrar las uris de publicaciones
        $tablaLocal.="<div class=\"table-responsive\"><table class=\"table table-striped table-bordered table-hover\"><thead class=\"thead-dark text-center\"><tr><th>Título</th><th>Link publicación</th></tr></thead><tbody>".PHP_EOL;
        foreach ($rl as $row){
            //$tablaLocal.="<tr><td>".$row['titulo']."</td><td><a href=".$row['plocales'].">".$row['plocales']."</a></td><td><a href=".$row['link'].">".$row['link']."</a></td></tr>".PHP_EOL;
                $tablaLocal.="<tr><td>".$row['titulo']."</td><td><a href=".$row['link'].">".$row['link']."</a></td></tr>".PHP_EOL;

            
        }
        $tablaLocal.="</tbody></table></div>".PHP_EOL;
    }
    else{
        $tablaLocal.="<p class=\"text-danger font-weight-bold\">No existen publicaciones locales</p>".PHP_EOL;
    }
    
    //query remota 
    $qr="PREFIX dc:<http://purl.org/dc/elements/1.1/> PREFIX foaf:<http://xmlns.com/foaf/0.1/> "
            . "SELECT ?uri ?title ?linkpdf WHERE { "
            . "?uri dc:creator <$dblpUri> ."
            . "?uri dc:title ?title ."
            . "?uri foaf:homepage ?linkpdf }";
    $rr= getRemota($qr);
   //print_r($rr);
   //echo strcmp($rr, "DBLP no responde");
   //if (!strcmp($rr, "DBLP no responde")!=0){
   //if($rr!==NULL){
        if(!empty($rr["result"]["rows"])){
            $tablaRemota.="<div class=\"table-responsive\"><table class=\"table table-striped table-bordered table-hover\"><thead class=\"thead-dark text-center\"><tr><th>Título</th><th>URI</th><th>Link publicación</th></tr></thead><tbody>".PHP_EOL;

            foreach ($rr["result"]["rows"] as $row){
                $tablaRemota.="<tr><td>".$row['title']."</td><td><a href=".$row['uri'].">".$row['uri']."</a></td><td><a href=".$row['linkpdf'].">".$row['linkpdf']."</a></td></tr>".PHP_EOL;
            }
            $tablaRemota.="</tbody></table></div>".PHP_EOL;
        }
        else{
            $tablaRemota.="<p class=\"text-danger font-weight-bold\">No existen publicaciones en DBLP para el investigador seleccionado";
        }
                                        //}
    //else{
        //$tablaRemota.="<p class=\"text-danger font-weight-bold\">No se puede establecer comunicación con el terminal DBLP";
   // }
    
    //return $tablaLocal;
    return  $tablaLocal.$tablaRemota;
}


function getRemota($qr){
    $resultadoRemoto= null;
    $terminal = "https://dblp.l3s.de/d2r/sparql";
    $fuente = new SparqlClient();
    $fuente->setEndpointRead($terminal);
    //$consulta= "PREFIX dc: <http://purl.org/dc/elements/1.1/> SELECT *  WHERE {?s dc:creator <https://dblp.l3s.de/d2r/resource/authors/Laura_Andrea_Cecchi> } LIMIT 5";
    $filas=$fuente->query($qr,'rows');
    $error= $fuente->getErrors();
    if($error){
               print_r($error);
               throw new Exception(print_r($error, true));
               //return "DBLP no responde";
               }
    /*else{
        return $filas;
    }*/
    /*else{
        var_dump($filas);
        $resultadoRemoto=$filas;
         //var_dump($filas);//modificar la forma de ver las filas.
        if(empty($filas['result']['rows'])){
            echo("Resultado remoto es null");
        }
        else{
            echo("Resultado remoto no es null");
        }
         } */
    
    return $filas;
}