<!DOCTYPE html>
<html> 
<head><title>Vista</title></head>
    <body>
<?php
  error_reporting(E_ALL);
            ini_set('display_errors', '1');
             
            //Linkeamos el Composer para que cargue las librerías de ARC2 y de BorderCloud
            use BorderCloud\SPARQL\SparqlClient; //esto es necesario para que nuestro php Utilice el objeto SparqlClient()
            require __DIR__ . '/vendor/autoload.php';
            
            //incluimos sparqllib para utilizar alguna funcionalidad de esta libreria
            include('sparqllib.php');
            
            
            /*ARC2 simula una base de datos RDF utilizando una base de datos relacional para ello primero debemos 
             * crear una base de datos en nuestro gestor de base de datos relacional local. El gestor debe ser MySQL o
             * MARIADB, no admite MySQL por encima de la version 8.0.
             * 1- Cargaremos en un arreglo en la variable $config, toda la información referida a nuestra base de datos local en este caso llamada opendb.
             */
            $config = array(
            'db_host'=>'127.0.0.1',                  //localhost
            'db_name'=>'opendb',                     //nombre de la base de datos
            'db_user'=>'root',                       //usuario de la base de datos
            'db_pwd'=>'aterrador00',                 //contraseña de la base de datos
            'store_name'=>'arc_tests',               //nombre de almacenamiento de triplas
            'bnode_prefix'=>'bn',                    //prefijo del nodo raíz o base
            'sem_html_formats'=>'rdfa microformats', //formato de los elementos a almacenar
                );
    
            /*2- Referencio la base de datos a un objeto Store en la variable $store y la enciendo si aún no lo está. */
            $store = ARC2::getStore($config);
            if(!$store->isSetUp())
                {
                    $store->setUp();
                }
                
            /*3- Este método se ejecuta una sola vez para cargar la información contenida en el archivo creado en OpenRefine, pruebaturtle.ttl, en
             *la base de datos local. La función corresponde a la función SPARQL LOAD que carga un archivo a la base de datos RDF. ARC2 la simula muy 
             *bien*/
            $q= 'LOAD <http://localhost/faienlazada/investigadores_publicaciones.ttl>';
            $store->query($q);
            $q= 'LOAD <http://localhost/faienlazada/publicacionesturtle.ttl>';
            $store->query($q);
?>
</body>
</html>