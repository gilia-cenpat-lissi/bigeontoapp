<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel='stylesheet' href='css/bootstrap.min.css'>
<link rel='stylesheet' href='css/micss.css'>
<title>FAI Enlazada</title>
<!--En esta página se realiza el formulario para llenar y hacer la consulta-->
</head>
<body>    
        <?php
            //error_reporting(E_ALL);
            //ini_set('display_errors', '1');
             
            //Linkeamos el Composer para que cargue las librerías de ARC2 y de BorderCloud
            use BorderCloud\SPARQL\SparqlClient; //esto es necesario para que nuestro php Utilice el objeto SparqlClient()
            require __DIR__ . '/vendor/autoload.php';
            require_once 'consultas.php';
        
        ?>
    <div class="row">
        <div class="column"><img src="src/logofai.png" class="logo"/></div>
        <div class="column"><h1>FAI Enlazada</h1></div> 
        <div class="column"><img src="src/comahue.png" class="logo"/></div>
    </div>
    <hr>
        <h2 class="font-weight-bold text-uppercase">Elija el investigador a consultar</h2>
        <form name="add" method="post">
        <p class="font-weight-bold">Autor: <?= getSelectoraAutores()?></p>
        <input type="submit" name="mostrar" value="Mostrar" class="btn btn-primary"/>
        <!--button id="resultados" click="resultados()">Mostrar</button-->
        </form>
        <br>
        <br>
        <h4 class="indicaciones subrallado font-weight-bold ">RESULTADOS:</h4>
        <?php
            $valor= explode("_",$_POST['select']);    
        ?>
        <p class="indicaciones"><?="Investigador: ".$valor[1]?></p>
        <p class="indicaciones"><?="URI Local: ".$valor[0].PHP_EOL;?></p>
        <p><?= getPublicaciones($valor[0]);?></p>
</body>    	

</html>